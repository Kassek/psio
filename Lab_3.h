//
// Created by piotr on 16.12.2021.
//
#include <iostream>
#include <vector>
#include <list>
#include <random>
#include <ctime>
#include <algorithm>

#ifndef LAB3_LAB_3_H
#define LAB3_LAB_3_H

using namespace std;
class Lab_3 {

    int n;
    vector<int> table;
    list<int> lista;

    static int randomInt(int min, int max);

    void resetData();

    void print_vector();

    void print_list();

public:
    Lab_3(int n = 10);

    void zad_1();

    void zad_2();

    void zad_3();

    void zad_4();

    void zad_5();

    void zad_6();
};


#endif //LAB3_LAB_3_H
