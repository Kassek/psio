//
// Created by piotr on 16.12.2021.
//

#include "Lab_3.h"

using namespace std;

int Lab_3::randomInt(int min, int max) {
    static default_random_engine e{static_cast<long unsigned int>(time(0))};
    uniform_int_distribution<int> d{min, max};
    return d(e);
}

void Lab_3::resetData() {
    this->table.clear();
    this->lista.clear();
    for(int i = 0; i < this->n; i++){
        this->table.push_back(randomInt(-20,20));
        this->lista.push_back(randomInt(-20,20));
    }
}

void Lab_3::print_vector() {
    for(auto i : this->table){
        cout<<i<<" ";
    }
}

void Lab_3::print_list() {
    for(auto i : this->lista){
        cout<<i<<" ";
    }
}

Lab_3::Lab_3(int n) {
    this->n = n;
    resetData();
}

void Lab_3::zad_1() {
    cout<<"zad_1\n";
    int x;

    for(int i = 0; i < this->n; i++){
        cout<<" "<<this->table[i];
    }

    cout<<"\nPodaj wartosc jaka chcesz usunac: "; cin>>x;

    for(auto i = this->table.begin(); i != this->table.end(); ++i){
        if(*i == x){
            this->table.erase(remove(this->table.begin(), this->table.end(), x), this->table.end());
        }
    }

    for(auto i = this->table.begin(); i != this->table.end(); ++i){
        cout<<" "<<*i;
    }
}

void Lab_3::zad_2() {
    cout<<"zad_2\n";

    int x_list;

    print_list();

    cout<<"\nPodaj wartosc jaka chcesz usunac: "; cin>>x_list;

    for(auto i = this->lista.begin(); i != this->lista.end(); ++i){
        if(*i == x_list){
            this->lista.erase(remove(this->lista.begin(), this->lista.end(), x_list), this->lista.end());
        }
    }

    for(auto i = this->lista.begin(); i != this->lista.end(); ++i){
        cout<<" "<<*i;
    }
}

void Lab_3::zad_3() {
    resetData();

    int x;

    cout<<"Vector:\n";
    print_vector();

    cout<<"\nPodaj wartosc jaka chcesz usunac: "; cin>>x;

    for (auto et = this->table.begin(); et != this->table.end(); et++){
        auto fnd = find(this->table.begin(),this->table.end(), x);
        if (*fnd == x){
            this->table.erase(fnd);
            et--;
        }
    }

    print_vector();

    // Lista
    cout<<"\n\nLista:\n";

    print_list();

    cout<<"\nPodaj wartosc jaka chcesz usunac: "; cin>>x;

    for (auto et = this->lista.begin(); et != this->lista.end(); et++){
        auto fnd = find(this->lista.begin(),this->lista.end(), x);
        if (*fnd == x){
            this->lista.erase(fnd);
            et--;
        }
    }

    print_list();
}

void Lab_3::zad_4() {
    cout<<"Vector:\n";

    auto min_max = minmax_element(this->table.begin(), this->table.end());

    cout<<"Wartosc maksymalna (max_element): "<<*max_element(this->table.begin(), this->table.end())<<'\n';
    cout<<"Wartosc minimalna (min_element): "<<*min_element(this->table.begin(), this->table.end())<<'\n';
    cout<<"Wartosc maksymalna (minmax_element): "<<*min_max.first<<", wartosc minimalna (minmax_element): "<<*min_max.second;

    cout<<"\n\nLista:\n";

    auto min_max_list = minmax_element(this->lista.begin(), this->lista.end());

    cout<<"Wartosc maksymalna (max_element): "<<*max_element(this->lista.begin(), this->lista.end())<<'\n';
    cout<<"Wartosc minimalna (min_element): "<<*min_element(this->lista.begin(), this->lista.end())<<'\n';
    cout<<"Wartosc maksymalna (minmax_element): "<<*min_max_list.first<<", wartosc minimalna (minmax_element): "<<*min_max_list.second;
}

void Lab_3::zad_5() {
    // Vector
    cout<<"Vector: Rosnaco:\n";
    sort(this->table.begin(), this->table.end());
    print_vector();

    cout<<"\nVector: Malejaco:\n";
    sort(this->table.begin(), this->table.end(), [](int x, int y){
        return x > y;
    });
    print_vector();

    cout<<"\nVector: Rosnaco bezwzglednie:\n";
    sort(this->table.begin(), this->table.end(), [](int x, int y){
        return abs(x) < abs(y);
    });
    print_vector();

    cout<<"\nVector: Malejaco bezwzglednie:\n";
    sort(this->table.begin(), this->table.end(), [](int x, int y){
        return abs(x) > abs(y);
    });
    print_vector();

    // Lista
    cout<<"\n\nLista: Rosnaco:\n";
    this->lista.sort();
    print_list();

    cout<<"\nLista: Malejaco:\n";
    this->lista.sort([](int x, int y){
        return x > y;
    });
    print_list();

    cout<<"\nLista: Rosnaco bezwzglednie:\n";
    this->lista.sort([](int x, int y){
        return abs(x) < abs(y);
    });
    print_list();

    cout<<"\nLista: Malejaco bezwzglednie:\n";
    this->lista.sort([](int x, int y){
        return abs(x) > abs(y);
    });
    print_list();
}

void Lab_3::zad_6() {
    // Nie zdążyłem :c
}